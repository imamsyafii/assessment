# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Database: bobobox_db
# Generation Time: 2022-03-21 17:08:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Hotel
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Hotel`;

CREATE TABLE `Hotel` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Hotel` WRITE;
/*!40000 ALTER TABLE `Hotel` DISABLE KEYS */;

INSERT INTO `Hotel` (`id`, `hotel_name`, `address`)
VALUES
	(1,'Bobobox Alun Alun Malang','Jl. Merdeka Selatan No.5, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia'),
	(2,'Bobobox Cipaganti Bandung','Jl. Cipaganti No.14, Pasir Kaliki, Cicendo, Kota Bandung, Jawa Barat 40171, Indonesia'),
	(3,'Bobobox Mega Bekasi','Jl. Jend. Ahmad Yani, RT.004/RW.001, Marga Jaya, Kec. Bekasi Bar., Kota Bks, Jawa Barat 17141, Indonesia');

/*!40000 ALTER TABLE `Hotel` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Price`;

CREATE TABLE `Price` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `room_type_id` bigint(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Price` WRITE;
/*!40000 ALTER TABLE `Price` DISABLE KEYS */;

INSERT INTO `Price` (`id`, `date`, `room_type_id`, `price`)
VALUES
	(1,'2020-01-10',1,100000),
	(2,'2020-01-10',2,120000),
	(3,'2020-01-11',1,110000),
	(4,'2020-01-10',2,125000);

/*!40000 ALTER TABLE `Price` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Promo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Promo`;

CREATE TABLE `Promo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `promo_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `promo_type` enum('AMOUNT','PERCENTAGE') DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `promo_start` date DEFAULT NULL,
  `promo_end` date DEFAULT NULL,
  `status` enum('1','0') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Promo` WRITE;
/*!40000 ALTER TABLE `Promo` DISABLE KEYS */;

INSERT INTO `Promo` (`id`, `promo_name`, `description`, `promo_type`, `amount`, `promo_start`, `promo_end`, `status`)
VALUES
	(1,'Bobo Birth Promo','Special for Bobobox\'s Birthday Promo','AMOUNT',7000,NULL,NULL,'1'),
	(2,'Bobo Surprize Day','Surprize Promo from Bobobox','PERCENTAGE',5,NULL,NULL,'1');

/*!40000 ALTER TABLE `Promo` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table promo_redeem
# ------------------------------------------------------------

DROP TABLE IF EXISTS `promo_redeem`;

CREATE TABLE `promo_redeem` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `redeem_date` date DEFAULT NULL,
  `promo_id` bigint(20) DEFAULT NULL,
  `price_before` float DEFAULT NULL,
  `price_after` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table PromoRoom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `PromoRoom`;

CREATE TABLE `PromoRoom` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `promo_id` bigint(20) DEFAULT NULL,
  `room_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `PromoRoom` WRITE;
/*!40000 ALTER TABLE `PromoRoom` DISABLE KEYS */;

INSERT INTO `PromoRoom` (`id`, `promo_id`, `room_id`)
VALUES
	(1,1,1),
	(2,1,2),
	(3,2,1);

/*!40000 ALTER TABLE `PromoRoom` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Reservation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Reservation`;

CREATE TABLE `Reservation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `booked_room_count` int(11) DEFAULT NULL,
  `checkin_date` date DEFAULT NULL,
  `checkout_date` date DEFAULT NULL,
  `hotel_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Reservation` WRITE;
/*!40000 ALTER TABLE `Reservation` DISABLE KEYS */;

INSERT INTO `Reservation` (`id`, `order_id`, `customer_name`, `booked_room_count`, `checkin_date`, `checkout_date`, `hotel_id`)
VALUES
	(1,1,'Imam Syafii',1,'2020-01-10','2020-01-12',1);

/*!40000 ALTER TABLE `Reservation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Room
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Room`;

CREATE TABLE `Room` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) DEFAULT NULL,
  `room_type_id` bigint(20) DEFAULT NULL,
  `room_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Room` WRITE;
/*!40000 ALTER TABLE `Room` DISABLE KEYS */;

INSERT INTO `Room` (`id`, `hotel_id`, `room_type_id`, `room_number`)
VALUES
	(1,1,1,101),
	(2,1,1,102),
	(3,1,3,301);

/*!40000 ALTER TABLE `Room` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table RoomType
# ------------------------------------------------------------

DROP TABLE IF EXISTS `RoomType`;

CREATE TABLE `RoomType` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `RoomType` WRITE;
/*!40000 ALTER TABLE `RoomType` DISABLE KEYS */;

INSERT INTO `RoomType` (`id`, `name`)
VALUES
	(1,'Sky Single'),
	(2,'Earth Single'),
	(3,'Sky Double'),
	(4,'Earth Double');

/*!40000 ALTER TABLE `RoomType` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table Stay
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Stay`;

CREATE TABLE `Stay` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` bigint(20) DEFAULT NULL,
  `guest_name` varchar(255) DEFAULT NULL,
  `room_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `Stay` WRITE;
/*!40000 ALTER TABLE `Stay` DISABLE KEYS */;

INSERT INTO `Stay` (`id`, `reservation_id`, `guest_name`, `room_id`)
VALUES
	(1,1,'Imam Syafii',3);

/*!40000 ALTER TABLE `Stay` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table StayRoom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `StayRoom`;

CREATE TABLE `StayRoom` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `stay_id` bigint(20) DEFAULT NULL,
  `room_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `StayRoom` WRITE;
/*!40000 ALTER TABLE `StayRoom` DISABLE KEYS */;

INSERT INTO `StayRoom` (`id`, `stay_id`, `room_id`, `date`)
VALUES
	(1,1,3,'2020-01-10'),
	(2,1,3,'2020-01-11');

/*!40000 ALTER TABLE `StayRoom` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
