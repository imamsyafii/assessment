<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Stay".
 *
 * @property int $id
 * @property int|null $reservation_id
 * @property string|null $guest_name
 * @property int|null $room_id
 */
class Stay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Stay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reservation_id', 'room_id'], 'integer'],
            [['guest_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reservation_id' => 'Reservation ID',
            'guest_name' => 'Guest Name',
            'room_id' => 'Room ID',
        ];
    }
}
