<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Reservation".
 *
 * @property int $id
 * @property int|null $order_id
 * @property string|null $customer_name
 * @property int|null $booked_room_count
 * @property string|null $checkin_date
 * @property string|null $checkout_date
 * @property int|null $hotel_id
 */
class Reservation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Reservation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'booked_room_count', 'hotel_id'], 'integer'],
            [['checkin_date', 'checkout_date'], 'safe'],
            [['customer_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'customer_name' => 'Customer Name',
            'booked_room_count' => 'Booked Room Count',
            'checkin_date' => 'Checkin Date',
            'checkout_date' => 'Checkout Date',
            'hotel_id' => 'Hotel ID',
        ];
    }
}
