<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Room".
 *
 * @property int $id
 * @property int|null $hotel_id
 * @property int|null $room_type_id
 * @property int|null $room_number
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Room';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hotel_id', 'room_type_id', 'room_number'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotel_id' => 'Hotel ID',
            'room_type_id' => 'Room Type ID',
            'room_number' => 'Room Number',
        ];
    }
    
    public static function getRoomList($req)
    {
	    $roomlist =  Room::find()
	    			->where("room_type_id=:roomtypeid",["roomtypeid"=>$req['room_type_id']])
	    			->asArray()
	    			->all();
		$data = [];
		foreach($roomlist as $rk=>$rv)
		{	
			$data[$rk] = [
			'id' => $rv['id'],
			'room_number' => $rv['room_number'],
			'price' => Price::find()
			->select("`date`, `price`")
			->where("room_type_id=:roomtypeid and date>=:datefrom and date<:dateto ",
			[
				'roomtypeid'=>$rv['room_type_id'],
				'datefrom' => $req['checkin_date'],
				'dateto' => $req['checkout_date'],
			])
			->asArray()
			->all()
			];
			
		}
	   return $data;
    }
    
    public static function promocalc($real_price, $promo_type, $promo_val)
    {
	    $return = 0;
	    switch($promo_type)
	    {
		    case "AMOUNT":
		    $return = $real_price - $promo_val;
		    break;
		    case "PERCENTAGE":
		    $return = $real_price - ($real_price*$promo_val/100);
		    break;
		}
	    
	    return $return;
    }
    
    public static function getPromoRoomList($roomlist, $promo_id, $total)
    {
	    $promoval = 0;
		$promo = Promo::find()->where("id=:pid",["pid"=>$promo_id])->asArray()->one();
		
		
		foreach($roomlist as $rk=>$rv)
		{	
			foreach($rv as $pricekey=>$priceval)
			{
				if($pricekey == "price")
				foreach($roomlist[$rk][$pricekey] as $detpricekey => $detpriceval)
				{
					if(!empty($promo))
					$roomlist[$rk][$pricekey][$detpricekey]['price'] = Room::promocalc($roomlist[$rk][$pricekey][$detpricekey]['price'], $promo['promo_type'], $promo['amount']) ;
				}
				
			}
		}
	   return $roomlist;
    }
}
