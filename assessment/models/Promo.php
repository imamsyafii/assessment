<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Promo".
 *
 * @property int $id
 * @property string|null $promo_name
 * @property string|null $description
 * @property string|null $promo_type
 * @property float|null $amount
 * @property string|null $promo_start
 * @property string|null $promo_end
 * @property string|null $status
 */
class Promo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Promo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promo_type', 'status'], 'string'],
            [['amount'], 'number'],
            [['promo_start', 'promo_end'], 'safe'],
            [['promo_name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_name' => 'Promo Name',
            'description' => 'Description',
            'promo_type' => 'Promo Type',
            'amount' => 'Amount',
            'promo_start' => 'Promo Start',
            'promo_end' => 'Promo End',
            'status' => 'Status',
        ];
    }
}
