<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Price".
 *
 * @property int $id
 * @property string|null $date
 * @property int|null $room_type_id
 * @property float|null $price
 */
class Price extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['room_type_id'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'room_type_id' => 'Room Type ID',
            'price' => 'Price',
        ];
    }
}
