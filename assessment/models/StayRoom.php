<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "StayRoom".
 *
 * @property int $id
 * @property int|null $stay_id
 * @property int|null $room_id
 * @property string|null $date
 */
class StayRoom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'StayRoom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stay_id', 'room_id'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stay_id' => 'Stay ID',
            'room_id' => 'Room ID',
            'date' => 'Date',
        ];
    }
}
