<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PromoRoom".
 *
 * @property int $id
 * @property int|null $promo_id
 * @property int|null $room_id
 */
class PromoRoom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'PromoRoom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promo_id', 'room_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_id' => 'Promo ID',
            'room_id' => 'Room ID',
        ];
    }
}
