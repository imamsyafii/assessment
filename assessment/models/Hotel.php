<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Hotel".
 *
 * @property int $id
 * @property string|null $hotel_name
 * @property string|null $address
 */
class Hotel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'Hotel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hotel_name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotel_name' => 'Hotel Name',
            'address' => 'Address',
        ];
    }
}
