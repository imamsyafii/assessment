<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;


class ApiController extends Controller
{
	public $enableCsrfValidation = false;
	
	public function beforeAction($action)
	{
	    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	      return parent::beforeAction($action);
	}
	
    public function actionIndex()
    {   
		return $this->asJson([1]);
    }
    
    public function actionRoomList()
    {
		$req = Yii::$app->request->post();
		$rooms = [];
		$total_price = 0;
		$request = [
			'room_qty' => isset($req['room_qty'])?(float)$req['room_qty']:0,
			'room_type_id' => isset($req['room_type_id'])?(float)$req['room_type_id']:0,
			'checkin_date' => isset($req['checkin_date'])?$req['checkin_date']:0,
			'checkout_date' => isset($req['checkout_date'])?$req['checkout_date']:0,
			//'total_price' => isset($req['total_price'])?(float)$req['total_price']:0,
			];
			
		$response = $request;
		$rooms = \app\models\Room::getRoomList($request);
		foreach($rooms as $rk=>$rv)
		{
			$prices = $rv['price'];
			foreach ($prices as $pk => $pv)
			{
				$total_price += $pv['price'];
			}
		}
		
		$response['total_price'] = $total_price;
		$response['available_room'] = $rooms;
		
		return $this->asJson($response);
    }
    
    public function actionPromoRoomList()
    {
		$req = Yii::$app->request->post();
		$rooms = [];
		$total_price = 0;
		$request = [
			'room_qty' => isset($req['room_qty'])?(float)$req['room_qty']:0,
			'room_type_id' => isset($req['room_type_id'])?(float)$req['room_type_id']:0,
			'checkin_date' => isset($req['checkin_date'])?$req['checkin_date']:0,
			'checkout_date' => isset($req['checkout_date'])?$req['checkout_date']:0,
			'promo_id' => isset($req['promo_id'])?(float)$req['promo_id']:0,
			//'total_price' => isset($req['total_price'])?(float)$req['total_price']:0,
			];
			
		$response = $request;
		$reg_rooms = \app\models\Room::getRoomList($request);
		foreach($reg_rooms as $rk=>$rv)
		{
			$prices = $rv['price'];
			foreach ($prices as $pk => $pv)
			{
				$total_price += $pv['price'];
			}
		}

		$promo_id = $request['promo_id'];
		$promo_rooms = \app\models\Room::getPromoRoomList($reg_rooms,$promo_id, $total_price);
		
		
		$response['total_price'] = $total_price;
		$response['available_room'] = $promo_rooms;
		
		return $this->asJson($response);
    }

}
